#### Contribution
Pull Requests always welcome.

## Customization

Edit this files before using

- _config.yml REQUIRED
- /index.html - change the default image and email
- /projects.html - here you can share some of your projects
- /resume.html - write something about you
- /favicon.ico change it or leave the existing one
- _includes/title.html REQUIRED

## Licensing

[MIT](https://github.com/railsr/autm-rb/blob/master/LICENSE)


[pages]: http://pages.github.com
[fork]: https://github.com/railsr/autm-rb/fork
[demo]: http://railsr.github.io/autm-rb/
